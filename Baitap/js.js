/**Bài 1: Tiền lương nhân viên
 * - Đầu vào: 
 * Lương 1 ngày: 100.000
 * Số ngày làm: 30 ngày
 * - Đầu ra:
 * Tiên lương của nhân viên = 3000000
 */
 var soNgay = 30;
 var soLuong = 100000;
 var luongTong = soNgay * soLuong;
 console.log(``,luongTong);
 /** Bài 2: Tính giá trị trung bình
  * - Đầu vào:
  * Nhập 5 số thực
  * num1 = 20
  * num2 = 15
  * num3 = 30
  * num4 = 75
  * num5 = 65
  * - Đầu ra 
  * Trung bình = 41
  */
 var num1 = 20;
 var num2 = 15;
 var num3 = 30;
 var num4 = 75;
 var num5 = 65;
 var trungBinh = ( num1 + num2 + num3 + num4 + num5)/5;
 console.log(``,trungBinh);
 /**Bài 3: Quy đổi tiền
  * - Đầu ra
  * Gán thẻ usa
  * Gán thẻ vnd
  * - Đầu ra
  * vnd = usd * 23500.
  */
 var usaEl = document.getElementById("usa");
 var vndEl= document.getElementById("vnd");
 function quyDoi(){
     console.log(vndEl.value=usaEl.value *23500)
 }
 /**Bài 4: Tính chu vi, diện tích HCN
  * - Đầu vào:
  * Chiều dài : 7
  * Chiều rộng: 5
  * - Đầu ra:
  * Diện tích: 35
  * Chu vi: 24
  */
 var chieuDai = 7 ;
 var chieuRong = 5;
 var dienTich = chieuDai * chieuRong;
 var chuVi = (chieuDai + chieuRong) * 2;
 console.log(`DienTich`, dienTich);
 console.log(`ChuVi`, chuVi);
 /**Bài 5: Tính tổng 2 kí số vừa nhập
  * - Đầu vào
  * Cho giá trị = 24
  * Chia cho 10 lấy dư 
  * Chia cho 10 làm tròn xuống 
  * - Đầu ra 
  * Kết quả = 6
  */
 var number = 24;
 var donVi= number % 10; 
 var hangChuc= Math.floor(number/10); 
 var result= donVi + hangChuc;
 console.log(``, result)
 